﻿using System.Data;

namespace VGCReport
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable soldData = VGCTool.GetVGCSoldReport();
            DataTable availableData = VGCTool.GetVGCAvailableReport();

            const string GS_EMAIL_HOST = "gsmail.babgsetc.pvt";
            string[] strReceipentTo = new string[] {
                "CORPInventoryAlertsVGC@gamestop.com"
               //"raviyarra@gamestop.com"
            }; 
            string[] recipientsCC = new string[]{
                "DLCSupport@gamestop.com",
                "GiftCards@gamestop.com"
            };
            EmailSendConfigure configure = new EmailSendConfigure()
            {
                TOs = strReceipentTo,
                CCs =  recipientsCC,
                From = "Gamestop.comSupport@gamestop.com",
                Subject = $"Tokens for GameStop.com Order | Availability Report | {System.DateTime.Now.ToString("MM-dd-yyyy") }",
                
            };
            string sHtml =
               "<HTML>\n" +
              "<HEAD>\n" +
              "<TITLE>VGC report</TITLE>\n" +
              "</HEAD>\n" +
              "<BODY style='font-size: 9pt;font-family:Arial'>\n" +
              "<P>Dear All,</P>\n\n" +
              "<P>Please find below GameStop tokens availability in Allspark for today</P>\n" +
              VGCTool.CreateHtmlTable(availableData).ToString() + "\n" +
              "<P>Please find below GameStop sold tokens report for past 14 days</P>\n" +
              VGCTool.CreateHtmlTable(soldData).ToString() + "\n\n" +
              "<P>Thanks,<br>GameStop Merchandizing Team</P>" +
              "</BODY>\n" +
              "</HTML>";

            EmailContent content = new EmailContent()
            {
                IsHtml = true,
                Content = sHtml,
            };
            new EmailManager(GS_EMAIL_HOST).SendMail(configure, content, false);
        }
    }
}
