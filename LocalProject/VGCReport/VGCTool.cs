﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace VGCReport
{
    public class VGCTool
    {
        /// <summary>
        /// Pull out the virtual GameStop soldout tokens
        /// </summary>
        /// <returns>returns VGC soldout tokens</returns>
        public static DataTable GetVGCSoldReport()
        {
            DataTable vgcSoldData = new DataTable();
            using (SqlConnection conn = SqlClientFactory.Instance.CreateConnection() as SqlConnection)
            {
                conn.ConnectionString = "Data Source=tcp:AW1ESPDB04SQL04.gs.pvt, 5513;Initial Catalog=AllSpark_NA;Integrated Security=True;Persist Security Info=False;";
                SqlCommand cmd = SqlClientFactory.Instance.CreateCommand() as SqlCommand;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"use [AllSpark_NA]
                            declare @date date
                            declare @catalogid int
                            declare @allSparkID int
                            set @date = getdate()
                            set @allSparkID = 494375
                            set @catalogid = (select CatalogID from catalog where AllSparkID = @allSparkID)
                            select cast(DateCreatedUTC as date) SoldDate, count(cast(DateCreatedUTC as date)) as QuantitySold 
                            from[order](nolock) where CatalogID = @catalogid and cast(datecreatedutc as date) >= getdate() - 14 
                            and TokenID is not null
                            group by cast(DateCreatedUTC as date)
                            order by solddate desc";
                cmd.Connection = conn;
                conn.Open();

                vgcSoldData.Load(cmd.ExecuteReader());
            }

            return vgcSoldData;
        }

        /// <summary>
        /// Pull out the virtual GameStop available tokens
        /// </summary>
        /// <returns>returns VGC available tokens</returns>
        public static DataTable GetVGCAvailableReport()
        {
            DataTable vgcAvailableData = new DataTable();
            using (SqlConnection conn = SqlClientFactory.Instance.CreateConnection() as SqlConnection)
            {
                conn.ConnectionString = "Data Source=tcp:AW1ESPDB04SQL04.gs.pvt, 5513;Initial Catalog=AllSpark_NA;Integrated Security=True;Persist Security Info=False;";
                SqlCommand cmd = SqlClientFactory.Instance.CreateCommand() as SqlCommand;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT CAST( GETDATE() AS Date ) AS 'Date', COUNT(*) AS 'Available Qty'
                                     FROM dbo.Token t ( NOLOCK ) 
                                     WHERE t.AllSparkID = 494375 AND IsUsed = 0;";
                cmd.Connection = conn;
                conn.Open();

                vgcAvailableData.Load(cmd.ExecuteReader());
            }

            return vgcAvailableData;
        }

        /// <summary>
        /// formatting the mail and sent to required receipents
        /// </summary>
        /// <param name="solddata">Tokens sold out data</param>
        /// <param name="availableData">Tokens availability data</param>
        public static int SendMails(DataTable solddata, DataTable availableData)
        {
            bool IsSuccess;
            Application vgcApp = null;
            NameSpace vgcNS = null;
            MailItem vgcMsg = null;
            Recipients recipients = null;
            Recipient recipientTo = null;
            Recipient recipientCC = null;
            string strReceipentTo = "CORPInventoryAlertsVGC@gamestop.com"; //"CORPInventoryAlertsVGC@gamestop.com"
            string[] recipientsCC = new string[]{
                "DLCSupport@gamestop.com", 
                "GiftCards@gamestop.com"
            };
            try
            {
                //Create the Outlook application.
                vgcApp = new Application();

                // Get the NameSpace and Logon information.
                vgcNS = vgcApp.GetNamespace("mapi");

                // Log on by using a dialog box to choose the profile.
                vgcNS.Logon(Missing.Value, Missing.Value, true, true);

                // Create a new mail item.
                vgcMsg = (MailItem)vgcApp.CreateItem(OlItemType.olMailItem);

                // Set the subject.
                vgcMsg.Subject = "Tokens for GameStop.com Order | Availability Report |" + DateTime.Now.ToString("MM-dd-yyyy");

                // Set HTMLBody.
                String sHtml =
                "<HTML>\n" +
               "<HEAD>\n" +
               "<TITLE>VGC report</TITLE>\n" +
               "</HEAD>\n" +
               "<BODY style='font-size: 9pt;font-family:Arial'>\n" +
               "<P>Dear All,</P>\n\n" +
               "<P>Please find below GameStop tokens availability in Allspark for today</P>\n" +
               CreateHtmlTable(availableData).ToString() + "\n" +
               "<P>Please find below GameStop sold tokens report for past 14 days</P>\n" +
               CreateHtmlTable(solddata).ToString() + "\n\n" +
               "<P>Thanks,<br>GameStop Merchandizing Team</P>" +
               "</BODY>\n" +
               "</HTML>";
                vgcMsg.HTMLBody = sHtml;

                // add new recipietns to the e-mail
                recipients = vgcMsg.Recipients;
                //Set email To receipent
                recipientTo = (Recipient)recipients.Add(strReceipentTo);
                recipientTo.Type = (int)OlMailRecipientType.olTo;

                //Set email CC recipients
                foreach (string tempCC in recipientsCC)
                {
                    recipientCC = (Recipient)recipients.Add(tempCC);
                    recipientCC.Type = (int)OlMailRecipientType.olCC;
                }

                recipients.ResolveAll();

                // Send.
                vgcMsg.Send();
                IsSuccess = true;
            }
            // Simple error handling.
            catch (System.Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
                IsSuccess = false;
            }
            finally
            {
                // Log off.
                vgcNS.Logoff();

                // Clean up.
                vgcMsg = null;
                vgcNS = null;
                vgcApp = null;
                recipientCC = null;
                recipientTo = null;
                recipients = null;
            }
            return IsSuccess ? 0 : -1;
        }

        /// <summary>
        /// create HTML table from datatable
        /// </summary>
        /// <param name="dataTable">Soldout/availability table</param>
        /// <returns>returns HTML table in the StringBuilder format</returns>
        public static StringBuilder CreateHtmlTable(DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();
            //Table start.
            sb.Append("<table cellpadding='5' cellspacing='0' style='border: 1px solid #ccc;font-size: 9pt;font-family:Arial'>");

            //Adding HeaderRow.
            sb.Append("<tr>");
            foreach (DataColumn column in dataTable.Columns)
            {
                sb.Append("<th style='background-color: #B8DBFD;border: 1px solid #ccc'>" + column.ColumnName + "</th>");
            }
            sb.Append("</tr>");


            //Adding DataRow.
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn column in dataTable.Columns)
                {
                    if(column.ColumnName == "Date" || column.ColumnName == "SoldDate")
                    {
                        var datetime = Convert.ToDateTime(row[column.ColumnName].ToString());
                        sb.Append("<td style='width:100px;border: 1px solid #ccc'>" + datetime.ToShortDateString() + "</td>");
                    }
                    else
                    sb.Append("<td style='width:100px;border: 1px solid #ccc'>" + row[column.ColumnName].ToString() + "</td>");
                }
                sb.Append("</tr>");
            }

            //Table end.
            sb.Append("</table>");
            return sb;
        }
    }
}
