﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using System.Configuration;
using System.Threading;
using System.IO;

namespace SKUDriftReport
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Log("===================== Application started ===============");
            string filePath = "";
            try
            {

                DataTable dataTable = DataAccess.GetSKUFromBrian();
                if (dataTable.Rows.Count > 0)
                {
                    Logger.Log("Entering ExportSKUToExcel fuction.");
                    FileOperation.ExportSKUToExcel(dataTable);

                    Logger.Log("Renaming excel file.");
                    FileOperation.renameFile();   //renaming the file for today.
                    Logger.Log("Renaming of excel file completed.");
                    //sending email..
                    //Thread.Sleep(3000); //will sleep for 3 sec
                    string path = FileOperation.path;
                    filePath = @path + FileOperation.getFileName();
                   // Logger.Log("Sending Email.");
                    //sendemail(filePath);
                  //  Logger.Log("Email sent.");
                }
                else
                {
                    //send blank email inoforming the user that there are no records available for today.
                    filePath = "";
                    Logger.Log("Sending Email.");
                    sendemail(filePath);
                    Logger.Log("Email sent.");

                }
            }
            catch (Exception exc)
            {
                Logger.Log(exc.Message);
                sendErrorEmail(exc.Message);
                throw;
            }

            Logger.Log("Execution completed Successfully.");
        }

        private static void sendemail(string filepath)
        {
            try
            {
                const string GS_EMAIL_HOST = "gsmail.babgsetc.pvt";
                string[] strReceipentTo = new string[] {
                "nityanandyadav@gamestop.com"
                //,charlie@gamestop.com
               
            };
                string[] recipientsCC = new string[]{
                //"JonathanSchafer@gamestop.com"
                //"HilaryTrabert@gamestop.com",
                //"VinayChandrasekhar@gamestop.com",
                //"RaviYarra@gamestop.com",
                //"nityanandyadav@gamestop.com",
                //"shritishaw@gamestop.com",
                //"YugandharT@gamestop.com"
            };
                EmailSendConfigure configure = new EmailSendConfigure()
                {
                    TOs = strReceipentTo,
                    CCs = recipientsCC,
                    From = "MerchAutomation@gamestop.com",
                    Subject = $"SKU Drift Report",


                };

                string bodycontent = filepath != "" ? "<P>Kindly find the attached report for today.</P>\n" : "<P>There are no records available for today.</P>\n";
                string sHtml =
                   "<HTML>\n" +
                  "<HEAD>\n" +
                  "<TITLE>SKU Drift</TITLE>\n" +
                  "</HEAD>\n" +
                  "<BODY style='font-size: 9pt;font-family:Arial'>\n" +
                  "<P>Hi Charlie,</P>\n\n" +
                 bodycontent +
                  "<P>Thanks,<br>GameStop Merchandizing Team</P>" +
                  "</BODY>\n" +
                  "</HTML>";

                EmailContent content = new EmailContent()
                {
                    IsHtml = true,
                    Content = sHtml,
                    AttachFileName = filepath != "" ? filepath : null
                };
                new EmailManager(GS_EMAIL_HOST).SendMail(configure, content, false);

            }
            catch (Exception exc)
            {
                Logger.Log(exc.Message);
                throw;
            }
        }


        private static void sendErrorEmail(string errormessage)
        {
            try
            {
                const string GS_EMAIL_HOST = "gsmail.babgsetc.pvt";
                string[] strReceipentTo = new string[] {
                "nityanandyadav@gamestop.com"
               ,"YugandharT@gamestop.com"
            };
                string[] recipientsCC = new string[]{
              //"shritishaw@gamestop.com",
                //"GiftCards@gamestop.com",
                //"ErinFoster@gamestop.com",
            };
                EmailSendConfigure configure = new EmailSendConfigure()
                {
                    TOs = strReceipentTo,
                    CCs = recipientsCC,
                    From = "MerchAutomation@gamestop.com",
                    Subject = $"SKU Drift Report Error",


                };


                string sHtml =
                   "<HTML>\n" +
                  "<HEAD>\n" +
                  "<TITLE>SKU Drift</TITLE>\n" +
                  "</HEAD>\n" +
                  "<BODY style='font-size: 9pt;font-family:Arial'>\n" +
                  "<P>Hi Team,</P>\n\n" +
                 "<P>An error has occured while sending SkuDrift Report.Please do the needful.</P>\n\n" +
                 "<P>Exception details: </P>\n" +
                 errormessage +
                 "\n" +
                  "<P>Thanks,<br>GameStop Merchandizing Team</P>" +
                  "</BODY>\n" +
                  "</HTML>";

                EmailContent content = new EmailContent()
                {
                    IsHtml = true,
                    Content = sHtml,
                    AttachFileName = null
                };
                new EmailManager(GS_EMAIL_HOST).SendMail(configure, content, false);

            }
            catch (Exception exc)
            {
                Logger.Log(exc.Message);
                throw;
            }
        }
    }
}
