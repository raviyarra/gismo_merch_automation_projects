﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDriftReport
{
    public static class Logger
    {
        public static readonly string Logfile = ConfigurationManager.AppSettings["LogFilepath"];

        public static void Log(string strLogText)
        {  // Create a writer and open the file:
            StreamWriter log;
            if (!File.Exists(@Logfile))
                log = new StreamWriter(@Logfile);
            else
                log = File.AppendText(@Logfile);

            // Write to the file:
            log.WriteLine(DateTime.Now + "  " + strLogText);
            //log.WriteLine(strLogText);
            log.WriteLine();
            // Close the stream:
            log.Close();
        }
    }

}
