﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pervasive.Data.SqlClient;
namespace SKUDriftReport
{
    internal class DataAccess
    {
        /// <summary>
        /// Getting ProductSKU data from Pervasive inv1 and inv3 table
        /// </summary>
        /// <returns>Datatable  of sku</returns>
        public static DataTable GetSKUFromBrian()
        {
            Logger.Log("Entered into GetSKUFromBrian.");
            DataTable dataTableSKU = null;
            try
            {


                using (PsqlConnection conn = PsqlFactory.Instance.CreateConnection() as PsqlConnection)
                {
                    using (PsqlCommand cmd = PsqlFactory.Instance.CreateCommand() as PsqlCommand)
                    {
                        Logger.Log("Executing get query.");
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;
                        conn.ConnectionString = "Host=GV1HQPDB02.babgsetc.pvt;ServerDSN=BRIAN;UID=UserID;Password=password;";
                            
                        conn.Open();
                        Logger.Log("DBConnection opened. ");
                        cmd.CommandText = @"
                       select SKU, ITWFromW, ITWToW, WHLocation
                        from inv3 a
                        join inv1 b
                        on a.item = b.item
                        where ITWFromW < 0 OR ITWToW < 0
                        order by SKU;";

                        dataTableSKU = new DataTable();
                        dataTableSKU.Load(cmd.ExecuteReader());
                    }
                }
                Logger.Log("Data fetched successfully.");
                return dataTableSKU;
            }
            catch (Exception ex)
            {
                //Logger.Log(ex.Message);
                throw ex;
            }
        }
        
    }
}
