﻿using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Data;
using System;

using System.Configuration;
namespace SKUDriftReport
{
    class FileOperation
    {

        public static readonly string path = ConfigurationManager.AppSettings["Filepath"];

        public static bool ExportSKUToExcel(DataTable dataTable)
        {
            Logger.Log("Entered into ExportSKUToExcel fuction.");
            bool result = false;

            try
            {
                Excel.Application xlApp = new Excel.Application();

                if (xlApp == null)
                {
                    Logger.Log("Excel is not properly installed!!");
                    result = false;
                }

                xlApp.Visible = false;
                string filename = @path + getFileName();
                Logger.Log("Opening excel file.");
                Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filename, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                Excel.Sheets worksheets = xlWorkBook.Worksheets;

                var xlNewSheet = (Excel.Worksheet)worksheets.Add(worksheets[1], Type.Missing, Type.Missing, Type.Missing);

                xlNewSheet.Name = getSheetName();

                Logger.Log("Writting data to excel file.");

                //ExcelWorkSheet = ExcelWorkBook.Worksheets[0];
                int r = 1; // Initialize Excel Row Start Position  = 1
                           //Writing Columns Name in Excel Sheet
                for (int col = 1; col <= dataTable.Columns.Count; col++)
                    xlNewSheet.Cells[r, col] = dataTable.Columns[col - 1].ColumnName;

                r++; // increment to next row to write except header
                     //Writing Rows into Excel Sheet
                for (int row = 0; row < dataTable.Rows.Count; row++) //r stands for ExcelRow and col for ExcelColumn
                {
                    // Excel row and column start positions for writing Row=1 and Col=1
                    for (int col = 1; col <= dataTable.Columns.Count; col++)
                    {
                        if (dataTable.Columns[col - 1].ColumnName == "SKU")
                        {
                            xlNewSheet.Cells[r, col].NumberFormat = "@";
                        }

                        xlNewSheet.Cells[r, col] = dataTable.Rows[row][col - 1].ToString();
                    }

                    r++;
                }
                //Making auto fit
                xlNewSheet.Columns.AutoFit();
                Logger.Log("Data written to excel file. Formatting data.");
                //range is the range of the table
                Excel.Range range = xlNewSheet.UsedRange;
                foreach (Excel.Range cell in range.Rows[1].Cells)
                {
                    cell.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    cell.Font.Bold = true;
                }
                range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Weight = Excel.XlBorderWeight.xlThin;
                //range.PrintPreview();

                //xlNewSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //xlNewSheet.Select();


                xlWorkBook.Save();
                xlWorkBook.Close();

                xlApp.Quit();
                releaseObject(xlNewSheet);
                releaseObject(worksheets);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                Logger.Log("Export data to excel file successful.");
            }
            catch (Exception exception)
            {
                ///Logger.Log(exception.Message);
                throw exception;
            }
            return result;
        }


        private static void releaseObject(object obj)
        {
            try
            {
                Marshal.ReleaseComObject(obj);
                obj = null;

            }

            catch (Exception ex)

            {

                obj = null;
                throw ex;


            }

            finally
            {

                GC.Collect();
                GC.WaitForPendingFinalizers();

            }

        }

        public static string getSheetName()
        {
            //10_11_0140
            string sheetName = "";
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);
            sheetName = cstTime.ToString("MM_dd_hhmm");
            return sheetName;
        }

        public static bool renameFile()
        {
            bool isrenamed = false;
            try
            {

                string oldFileName = path + getFileName();
                string newFileName = path + generateNewFilName();

                // Create a FileInfo  
                FileInfo fi = new FileInfo(@oldFileName);
                // Check if file is there  
                if (fi.Exists)
                {
                    // Move file with a new name. Hence renamed.  
                    fi.MoveTo(@newFileName);
                    isrenamed = true;
                }

            }
            catch (Exception exception)
            {

                throw;
            }
            return isrenamed;
        }

        public static string getFileName()
        {
            string filename = "";
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.GetFiles().Count() == 0)
            {
                Logger.Log("No file Found.");

                return "";
            }

            filename = di.GetFiles()
           .Select(fl => fl.Name)
           .FirstOrDefault(name => name != "");

            return filename;
        }


        public static string generateNewFilName()
        {
            Logger.Log("Generating  new  file name.");
            //SKU_Drift_10 - 08
            string str = "SKU Drift " + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString();

            return str + ".xlsx";
        }


    }
}
