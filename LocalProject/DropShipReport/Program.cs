﻿using System.Collections.Generic;
using System.Data;

namespace DropShipReport
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dropShipdata = DropShipTool.GetDropShipReport();
            List<string> SKUs = DropShipTool.GetProductSKUs<string>(dropShipdata, "SKU");
            if(SKUs != null && SKUs.Count > 0)
            {
                DataTable locations = DropShipTool.GetWHLocationFromBrian(SKUs);

                if(locations != null && locations.Rows.Count > 0)
                {
                    //Sending mail
                    DropShipTool.SendMails(dropShipdata, locations, SKUs.Count);
                }
            }
        }
    }
}
