﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Office.Interop.Outlook;
using Pervasive.Data.SqlClient;

namespace DropShipReport
{
    public class DropShipTool
    {
        public static DataTable GetDropShipReport()
        {
            DataTable dropShipData = new DataTable();
            using (SqlConnection conn = SqlClientFactory.Instance.CreateConnection() as SqlConnection)
            {
                conn.ConnectionString = @"Data Source=tcp:GV1HQPDB28SQL02.babgsetc.pvt\INST02;Initial Catalog=Gismo;Integrated Security=True;Persist Security Info=False;";
                SqlCommand cmd = SqlClientFactory.Instance.CreateCommand() as SqlCommand;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"USE Gismo
                        ;WITH T
                        AS (
                            SELECT sku,
                                   lead(IsDropShip) OVER (PARTITION BY ID ORDER BY updatedate desc) AS Prev_IsDropShip,
                                   IsDropShip,
                                   lead(UpdateDate) OVER (PARTITION BY ID ORDER BY updatedate desc) AS Prev_UpdateDate,
                                   UpdateDate,
                                    ROW_NUMBER() over (Partition by ID order by updatedate desc) as RN
                            FROM dbo.ProductSKU FOR SYSTEM_TIME ALL  
                        )
                        SELECT T.SKU,
                                t.Prev_UpdateDate,
                               T.UpdateDate as LatestUpdateDate,       
                               C.ColName,
                               C.prev_value,
                               C.cur_value
                        FROM T
                            CROSS APPLY
                            (
                                VALUES
                                    ('IsDropShip', T.IsDropShip, T.Prev_IsDropShip)
                            ) AS C (ColName, cur_value, prev_value)
                        WHERE EXISTS
                        (
                            SELECT cur_value 
                            EXCEPT 
                            SELECT C.prev_value
                        ) and RN = 1 and Prev_UpdateDate is not null 
                        and UpdateDate >GETDATE() - 1 and prev_value = 1 and cur_value = 0
                        order by UpdateDate desc";
                cmd.Connection = conn;
                conn.Open();

                dropShipData.Load(cmd.ExecuteReader());
            }

            return dropShipData;
        }

        /// <summary>
        /// Filtering SKU's/VPN's/AllSparkId's from specific table
        /// </summary>
        /// <param name="tableData">data</param>
        /// <returns>List of SKU's/VPN's/AllSparkID's</returns>
        public static List<T> GetProductSKUs<T>(DataTable tableData, string field)
        {
            List<T> SKUs = new List<T>();
            if (tableData != null && tableData.Rows.Count > 0)
            {
                try
                {
                    SKUs = tableData.AsEnumerable()
                               .Select(r => r.Field<T>(field))
                               .ToList();

                }
                catch (System.Exception ex)
                { Console.WriteLine(ex.Message); }
            }

            return SKUs;
        }

        public static DataTable GetWHLocationFromBrian(List<string> skus)
        {
            DataTable allocationPurLoc = null;
            if (skus != null && skus.Count > 0)
            {
                using (PsqlConnection conn = PsqlFactory.Instance.CreateConnection() as PsqlConnection)
                using (PsqlCommand cmd = PsqlFactory.Instance.CreateCommand() as PsqlCommand)
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conn;
                    conn.ConnectionString = "Host=GV1HQPDB01.babgsetc.pvt;ServerDSN=BRIAN;UID=APIUser;Password=wbqrdabk;";
                    conn.Open();
                    cmd.CommandText = @"
                        select SKU, WHLocation as 'Allocation WH', 
                        WHLocation as 'Purchase WH' from INV1 
                        where WHLocation = 'OFF' and SKU in
                         (
                          " + GetFormatSKUsItem(skus) +
                         ")";

                    allocationPurLoc = new DataTable();
                    allocationPurLoc.Load(cmd.ExecuteReader());
                }
            }

            return allocationPurLoc;
        }

        /// <summary>
        /// formatting the mail and sent to required receipents
        /// </summary>
        /// <param name="data">data</param>
        public static void SendMails(DataTable data, DataTable locs, int count)
        {
            Application dlcApp = null;
            NameSpace dlcNS = null;
            MailItem dlcMsg = null;
            Recipients recipients = null;
            Recipient recipientTo = null;
            Recipient recipientCC = null;
            string strReceipentTo = "Group-HardwareSoftwareMerchandising@gamestop.com"; 
            string[] recipientsCC = new string[]{
                "GISMOSUPPORT@gamestop.com"
            };
            try
            {
                // Create the Outlook application.
                dlcApp = new Application();

                // Get the NameSpace and Logon information.
                dlcNS = dlcApp.GetNamespace("mapi");

                // Log on by using a dialog box to choose the profile.
                dlcNS.Logon(Missing.Value, Missing.Value, true, true);

                // Create a new mail item.
                dlcMsg = (MailItem)dlcApp.CreateItem(OlItemType.olMailItem);

                // Set the subject.
                dlcMsg.Subject = "Gismo DropShip Report | " + DateTime.Now.ToString("MM-dd-yyyy");

                // Set HTMLBody.
                String sHtml =
                "<HTML>\n" +
               "<HEAD>\n" +
               "<TITLE>Dropship report</TITLE>\n" +
               "</HEAD>\n" +
               "<BODY style='font-size: 9pt;font-family:Arial'>\n" +
               "<P>Dear All,</P>\n\n" +
               GetCountContent(count) +
               //GetMailContent(isMatched) +
               "\n<P><b>Please find the below details</b></P>\n" +
               CreateHtmlTable(data).ToString() + "\n" +
               "<P>For the below SKUs, the Allocation WH and purchase WH are been updated and still marked as OFF-Offsite. </P>\n\n" +
               CreateHtmlTable(locs).ToString() + "\n" +
               "<P>Kindly take a look on this and let us know if assistence is needed</P>\n" +
               "<P>Thanks,<br>GameStop Gismo Support</P>" +
               "</BODY>\n" +
               "</HTML>";
                dlcMsg.HTMLBody = sHtml;

                // add new recipietns to the e-mail
                recipients = dlcMsg.Recipients;
                //Set email To receipent
                recipientTo = (Recipient)recipients.Add(strReceipentTo);
                recipientTo.Type = (int)OlMailRecipientType.olTo;

                //Set email CC recipients
                foreach (string tempCC in recipientsCC)
                {
                    recipientCC = (Recipient)recipients.Add(tempCC);
                    recipientCC.Type = (int)OlMailRecipientType.olCC;
                }

                recipients.ResolveAll();

                // Send.
                dlcMsg.Send();
            }

            // Simple error handling.
            catch (System.Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            finally
            {
                // Log off.
                dlcNS.Logoff();

                // Clean up.
                dlcMsg = null;
                dlcNS = null;
                dlcApp = null;
                recipientCC = null;
                recipientTo = null;
                recipients = null;
            }

        }

        /// <summary>
        /// create HTML table from datatable
        /// </summary>
        /// <param name="dataTable">Soldout/availability table</param>
        /// <returns>returns HTML table in the StringBuilder format</returns>
        public static String CreateHtmlTable(DataTable dataTable)
        {
            if (dataTable == null || dataTable.Rows.Count == 0) return string.Empty;

            StringBuilder sb = new StringBuilder();
            //Table start.
            sb.Append("<table cellpadding='5' cellspacing='0' style='table-layout: auto !important;width:auto;border: 1px solid #ccc;font-size: 9pt;font-family:Arial'>");

            //Adding HeaderRow.
            sb.Append("<tr>");
            foreach (DataColumn column in dataTable.Columns)
            {
                sb.Append("<th style='width: auto !important;background-color: #B8DBFD;border: 1px solid #ccc'>" + column.ColumnName + "</th>");
            }
            sb.Append("</tr>");


            //Adding DataRow.
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn column in dataTable.Columns)
                {
                    if (column.ColumnName == "ReleaseDate" || column.ColumnName == "DateCreated")
                    {
                        var datetime = Convert.ToDateTime(row[column.ColumnName].ToString());
                        sb.Append("<td style='width: auto !important;border: 1px solid #ccc'>" + datetime.ToShortDateString() + "</td>");
                    }
                    else
                        sb.Append("<td style='width: auto !important;border: 1px solid #ccc'>" + row[column.ColumnName].ToString() + "</td>");
                }
                sb.Append("</tr>");
            }

            //Table end.
            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private static string GetCountContent(int count)
        {
            string content = null;
            if (count > 0)
            {
                 content = "<P>The DropShip status is updated for #" + count + " SKU on yesterday.</P>\n";
            }
            else
            {
                content = "<P>The DropShip status is not updated</P>\n";
            }
            return content;
        }

        /// <summary>
        /// Formatting the list of SKU's
        /// </summary>
        /// <param name="data">List of SKU's</param>
        /// <returns>Formatted SKU's</returns>
        private static string GetFormatSKUsItem(List<string> data)
        {
            string strConstruct = null;
            if (data != null && data.Count > 0)
            {
                for (int i = 0; data.Count > i; i++)
                {
                    strConstruct = strConstruct + "'" + data[i] + "'";
                    if (data.Count - 1 != i)
                        strConstruct = strConstruct + ",";
                }

            }
            return strConstruct;
        }


    }
}
